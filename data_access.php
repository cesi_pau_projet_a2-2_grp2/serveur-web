<?php

class DAO
{

	/**
	 * Name of the current user
	 */
	public static $surname = "<span style=\"color: red\">APPELER <code>get_user_current()</code> please</span>";
	public static $name = "<span style=\"color: red\">APPELER <code>get_user_current()</code> please</span>";
	public static $id = -1;
	public static $currentuser;
	
	/**
	 * Permission level of the current user
	 * 
	 * 0 => Guest
	 * 1 => Logged in user
	 * 2 => Students' Bureau member
	 * 3 => CESI employee
	 * 4 => I am (G)root
	 */
	public static $perm_level = 0;

	/**
	 * Gets information about the current user
	 */
	static function get_user_current()
	{
		if (isset($_COOKIE["BDE_token"])) {
			try {
				// Get the user from the database with the token
				$bdd = new mysqli("localhost", "ProjetA2-2", "CESI 0x424445", "a2_projet_2_test");
				$bdd->set_charset('utf8');
				if ($bdd->connect_errno) throw new Exception("Échec de la connexion à la BDD");
				$sql = "SELECT * FROM account WHERE token = \"".$_COOKIE["BDE_token"].'"';
				$res = $bdd->query($sql);
				if (!$res || $res->num_rows <= 0) {
					?>
					<div class="notification-error notification-floating">Votre jeton est invalide. Veuillez vous <a href="./Sign in.php">reconnecter</a></div>
					<?php
				}
				$row = $res->fetch_assoc();

				// Valid token 
				self::$surname = $row["surname"];
				self::$name = $row["name"];
				self::$perm_level = $row["status"];
				self::$id = $row["id_account"];
				self::$currentuser = $row;
			} catch (Exception $err) {
				// Invalid token
				self::$perm_level = 0;
				echo $err;
				setcookie("BDE_token", false);
			}
		} else
			self::$perm_level = 0;
	}

	static function restrict_access($whitelist) {
		$ok = false;
		foreach ($whitelist as $level) {
			if (self::$perm_level == $level) {
				$ok = true;
				break;
			}
		}
		if ($ok == false) { ?><script>window.location = "./unauthorized.php"</script> <?php }
			//{header("Location: ./unauthorized.php",true,401);}
	}
	/**
	 * Get the ideas box's contents
	 * 
	 * If the user is logged on, 
	 */
	static function get_ideas()
	{
		return EasyCURL::get("/ideas/".(isset(self::$currentuser) ? self::$currentuser["id_campus"] : 0),null)["body"];
	}

	/**
	 * Get an event's details
	 * @param int $id
	 */
	public static function get_event(int $id)
	{
		return EasyCURL::get("/event/$id",null)["body"];
	}

	/**
	 * Get a user's notifications
	 */
	public static function get_notifications()
	{
		return EasyCURL::get("/notifications/".self::$id, json_encode(["token"=>$_COOKIE["BDE_token"]]))["body"];
	}
	/**
	 * Get the events list from the database
	 */
	public static function get_events()
	{
		return EasyCURL::get("/events/".(isset(self::$currentuser) ? self::$currentuser["id_campus"] : 0),null)["body"];
	}
	/**
	 * Get the products list from the database
	 */
	public static function get_products()
	{
		return EasyCURL::get("/products",null)["body"];
	}
	/**
	 * Get the category list from the database
	 */
	public static function get_category()
	{
		return EasyCURL::get("/categories",null)["body"];
	}
}

/**
 * Allows for simple request sending to the API server
 */
class EasyCURL
{
	//public static $apiURL = "http://90.30.64.156:8001";
	public static $apiURL = "http://10.64.128.111:8001";

	/**
	 * Send a HTTP POST request
	 * @param string $path target URL
	 * @param string $content body of the request, should be in JSON.
	 * @return int Response HTTP status code, 0 if unreachable
	 */
	public static function post($path, $content)
	{
		$curl = curl_init(self::$apiURL.$path);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

		curl_exec($curl);

		return curl_getinfo($curl, CURLINFO_HTTP_CODE);
	}
	/**
	 * Send a HTTP DELETE request
	 * @param string $path target URL
	 * @param string $content body of the request, should be in JSON.
	 * @return int Response HTTP status code, 0 if unreachable
	 */
	public static function delete($path, $content)
	{
		$curl = curl_init(self::$apiURL.$path);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");

		curl_exec($curl);

		return curl_getinfo($curl, CURLINFO_HTTP_CODE);
	}
	/**
	 * Send a HTTP GET request, and retrieves the answer
	 * @param string $path target URL
	 * @param string $content body of the request, should be in JSON.
	 * @return array Response body decoded from JSON, and the HTTP status code (respectively ['body'] and ['code'])
	 */
	public static function get($path, $content)
	{
		$curl = curl_init(self::$apiURL.$path);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");

		$result = curl_exec($curl);
		return [ "code" => curl_getinfo($curl, CURLINFO_HTTP_CODE), "body" => json_decode($result)];
	}
}


?>