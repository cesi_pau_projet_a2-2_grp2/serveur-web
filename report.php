<?php
$title = "Liste des manifestations";
include ("./layout/header_nav.php");
$type = $_GET["type"];
$id = $_GET["id"];
$user = EasyCURL::get("/account/".$id, null)["body"];
?>

    <main>
        <?php if (isset($type,$id)) { ?>
            <h2>Vous êtes sur le point de signaler
            <?php    
                switch ($type) {
                    case 'idea': echo "l'idée'";  break;
                    case 'comm': echo "le commentaire";  break;
                    case 'pict': echo "l'image";  break;
                    case 'evnt': echo "l'événement";  break;
                    
                    default:
                        echo "TYPE INVALIDE";
                        break;
                }  
            ?> de <?= $user->name ?> <?= $user->surname ?>:</h2>
            <p><?php    
                // switch ($type) {
                //     case 'idea': echo "l'idée'";  break;
                //     case 'comm': echo "le commentaire";  break;
                //     case 'pict': echo "l'image";  break;
                //     case 'evnt': echo "l'événement";  break;
                    
                //     default:
                //         echo "TYPE INVALIDE";
                //         break;
                // }  
            ?></p>
            <label for="reason">Pourquoi cet élément est-il inapproprié ?</label><br/>
            <textarea type="text" id="reason" rows="5" cols="60" placeholder="Raison"></textarea><br/> <!-- Shows a wide area to type a reason of report. -->
            <button>ANNULER</button>
            <?php    
                switch ($type) {
                    case 'idea': ?><a onclick="reportidea()"><button>Signaler</button></a><?php  break;
                    case 'comm': ?><a onclick="reportcomm()"><button>Signaler</button></a><?php  break;
                    case 'pict': ?><a onclick="reportpict()"><button>Signaler</button></a><?php  break;
                    case 'evnt': ?><a onclick="reportevnt()"><button>Signaler</button></a><?php  break;
                    
                    default:
                        echo "TYPE INVALIDE";
                        break;
                }  
            ?>
        <?php } else { ?>
            <h2>Vous avez incorrectement appelé cette page.</h2>
            <code>Paramètres GET "type" et "id" manquants</code>
        <?php } ?>
    </main>
    
<?php include("./layout/footer.php")?>