<?php
$title = "Mentions légales";
include ("./layout/header_nav.php");
?>
    
    <main>
        <h1>MENTIONS LÉGALES</h1>
        <h2>Éditeur : Association des Bureaux des Étudiants des centres CESI<br/>
            Association CESI</h2>
        <p>SIREN : 775 722 572<br/>
            Siège social : <br/>
            30, Rue Cambronne<br/>
            75015 Paris<br/>
            Tél : 01 44 19 23 45<br/>
            Fax : 01 42 50 25 06<br/>
            e-mail : BDE-CESI@cesi.fr</p>

        <h2>Développement</h2>
        <p>Groupe Projet A2-2<br/>
            Théo De Ana, Damien Nouilhan, Jérémy Dussaux, Guillaume Sintes<br/>
            Centre CESI Pau Orbigny<br/>
            8, Rue des frères Charles et Alcide d'Orbigny<br/>
            64000 Pau</p>

        <h2>Hébergement</h2>
        <p>OVH<br/>
            2, Rue Kellermann<br/>
            59100 Roubaix<br/>
            09 72 10 47 46<br/>
            contact@ovh.com</p>

        <h2>Respect de la vie privée et collecte des Données Personnelles</h2>
        <p>Soucieux de protéger la vie privée de ses clients, les BDE CESI s'engagent dans la protection des données personnelles. Une politique sur la 
            protection des données personnelles rappelle nos principes et nos actions visant au respect de la règlementation applicable en matière de 
            protection des données à caractère personnel.<br/>
            <a href="PrivacyPolicy.php">Pour lire l'intégralité de notre politique sur la Protection des données personnelles, cliquez-ici</a></p>

        <h2>Sécurité</h2>
        <p>Les BDE du CESI s'engagent à mettre en oeuvre tous les moyens nécessaires au bon fonctionnement du site. Cependant, les BDE du CESI ne peuvent 
            pas garantir la continuité absolue de l'accès aux services proposés par le site. Les adhérents sont informés que les informations et 
            services proposés sur le site pourront être interrompus en cas de force majeure et pourront, le cas échéant, contenir des erreurs techniques.</p>
        
        <h2>Utilisation des cookies</h2>
        <p>Des cookies sont utilisés sur notre site.<br/>
            <a href="PrivacyPolicy.php">Pour plus d'informations, vous pouvez vous référer à la Politique sur la Protection des Données Personnelles 
                en cliquant-ici.</a></p>

        <h2>Décalaration d'activité</h2>
        <p>Bureaux des Étudiants des centres CESI - Association loi 1901<br/>
            775 722 572<br/>
            30, Rue Cambronne - 75015 Paris - France<br/>
            Tél : +33(0) 1 44 13 23 45 - Fax : +33(0) 1 42 50 25 06<br/>
            Déclaration d'activité enregistrée sous le numéro 11 75 47833 75 auprès du Préfet de la région Île-de-France<br/>
            Cet enregistrement ne vaut pas agrément de l'État.</p>

    </main>

<?php include("./layout/footer.php")?>