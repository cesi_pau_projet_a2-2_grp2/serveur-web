<?php
$title = "Paramètres utilisateur";
include ("./layout/header_nav.php");
?>
<!-- This page is created only to modify the password. With more time, we could improve the experience by allowing the user to modify other informations. -->
    <main>
        <h1>Changement du mot de passe</h1>
        <p><label for="current_password">Mot de passe actuel</label><br/>
        <input type="password" id="current_password" placeholder="Mot de passe actuel"></p>
        <p><label for="new_password">Nouveau mot de passe</label><br/>
        <input type="password" id="new_password" placeholder="Nouveau mot de passe"><br/>
        <label>Confirmation du nouveau mot de passe</label><br/>
        <input type="password" placeholder="Confirmez le mot de passe"></p>

        <a href="userHome.php"><button>ANNULER</button></a>
        <button>ENREGISTRER</button>
    </main>
    
<?php include("./layout/footer.php")?>