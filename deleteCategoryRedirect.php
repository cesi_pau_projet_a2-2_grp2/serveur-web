<?php
if (isset($_POST["deleting_category"])) {
    $category = $_POST["deleting_category"]; // The POST parameter is put into a variable 

    require_once("data_access.php");

    
        
    $content = json_encode(["token" => $_COOKIE["BDE_token"]]); // Translates the array into a JSON file which will be submitted to the API

        EasyCURL::delete("/category/".$category,$content); // Delete a category from the database
        header("Location: ./products.php"); // Shows the right page to the user
        
    
}
?>