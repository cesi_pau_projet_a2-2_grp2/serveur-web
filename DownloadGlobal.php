<?php
if (isset($_GET["filename1"])){ //Allow the downloading of the CSV file from the event-details page
    $filename1 = $_GET["filename1"];
    $uploadname1 = basename($filename1);

    header('Content-Transfer-Encoding: none');
    header('Content-Type: application/octetstream');
    header('Content-Disposition: attachment; filename = "'.$uploadname1.'"');
    header('Content-length: '.filesize($filename1));
    header("Pragma: no-cache");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
    header("Expires: 0");
    @readfile($filename1) OR die();
}

if (isset($_GET["filename2"])){ //Allow the downloading of the PDF file from the event-details page
    $filename2 = $_GET["filename2"];
    $uploadname2 = basename($filename2);

    header('Content-Transfer-Encoding: none');
    header('Content-Type: application/octetstream');
    header('Content-Disposition: attachment; filename = "'.$uploadname2.'"');
    header('Content-length: '.filesize($filename2));
    header("Pragma: no-cache");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
    header("Expires: 0");
    @readfile($filename2) OR die();
}

if (isset($_GET["filename3"])){ //Allow the downloading of the ZIP file with all pictures posted by BDE members and students
    $filename3 = $_GET["filename3"];
    $uploadname3 = basename($filename3);

    header('Content-Transfer-Encoding: none');
    header('Content-Type: application/octetstream');
    header('Content-Disposition: attachment; filename = "'.$uploadname3.'"');
    header('Content-length: '.filesize($filename3));
    header("Pragma: no-cache");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
    header("Expires: 0");
    @readfile($filename3) OR die();
}
?>