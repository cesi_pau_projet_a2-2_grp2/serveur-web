let form = document.getElementById('form');

function validateForm() {
  let lname = document.forms["form"]["surname"].value;
  let fname = document.forms["form"]["name"].value;
  let centre = document.forms["form"]["centre"].value;
  let mail = document.forms["form"]["email"].value;
  let password = document.forms["form"]["password"].value;
  let passwordConfirm = document.forms["form"]["confirm"].value;
  if (lname == ""){
    lname.setCustomValidity('Veuillez indiquer un nom');
    updateErrorMessage();
    return false;
  }
  if (fname == ""){
    fname.setCustomValidity('Veuillez indiquer un prénom');
    updateErrorMessage();
    return false;
  }
  if (centre == "0"){
    centre.setCustomValidity('Veuillez sélectionner un centre');
    updateErrorMessage();
    return false;
  }
  if (mail == ""){
    mail.setCustomValidity('Veuillez entrer une adresse e-mail');
    updateErrorMessage();
    return false;
  }
  if (password == ""){
    paswword.setCustomValidity('Veuillez entrer un mot de passe');
    updateErrorMessage();
    return false;
  }
  if (passwordConfirm == ""){
    paswwordConfirm.setCustomValidity('Veuillez confirmer votre mot de passe');
    updateErrorMessage();
    return false;
  }
    let updateErrorMessage = function() {
     document.getElementById('wrong_surname').innerHTML = lname.validationMessage;
     document.getElementById('wrong_name').innerHTML = fname.validationMessage;
     document.getElementById('centre_error').innerHTML = centre.validationMessage;
     document.getElementById('wrong_mail').innerHTML = mail.validationMessage;
     document.getElementById('password_error').innerHTML = password.validationMessage;
     document.getElementById('confirm_error').innerHTML = passwordConfirm.validationMessage;
    };
};


(function() {
  let password1 = document.getElementById('password');
  let password2 = document.getElementById('confirm');
  
  let checkPasswordValidity = function() {
      if (password1.value != password2.value) {
          password1.setCustomValidity('Les mots de passe doivent être identiques.');
          updateErrorMessage();
      } else {
          password1.setCustomValidity('');
      }        
  };
  
  let updateErrorMessage = function() {
      document.getElementById('password_error').innerHTML = password1.validationMessage;
  };
  
  password1.addEventListener('change', checkPasswordValidity, false);
  password2.addEventListener('change', checkPasswordValidity, false);
  
  form.addEventListener('submit', function(event) {
      if (form.classList) form.classList.add('submitted');
      checkPasswordValidity();
      if (!this.checkValidity()) {
          event.preventDefault();
          updateErrorMessage();
          password1.focus();  
      }
  }, false);
}());
