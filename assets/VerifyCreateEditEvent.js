let eventForm = document.getElementById('eventForm');

function validateEventForm(){
    let eName = document.forms["eventForm"]["event_name"].value;
    let eDate = document.forms["eventForm"]["date"].value;
    let eHour = document.forms["eventForm"]["hour"].value;
    let eCost = document.forms["eventForm"]["cost"].value;

    if (eName == "") {
        eName.setCustomValidity('Merci de renseigner le titre de la manifestation');
        updateErrorMessage();
        return false;
    }
    if (eDate == "") {
        eName.setCustomValidity('Merci de renseigner la date de la manifestation');
        updateErrorMessage();
        return false;
    }
    if (eHour == "") {
        eName.setCustomValidity("Merci de renseigner l'heure de la manifestation");
        updateErrorMessage();
        return false;
    }
    if (eCost == "") {
        eName.setCustomValidity('Merci de renseigner le prix de participation à la manifestation');
        updateErrorMessage();
        return false;
    }

    let updateErrorMessage = function() {
        document.getElementById('event_name_error').innerHTML = eName.validationMessage;
        document.getElementById('event_date_error').innerHTML = eDate.validationMessage;
        document.getElementById('event_hour_error').innerHTML = eHour.validationMessage;
        document.getElementById('event_cost_error').innerHTML = eCost.validationMessage;
       };

};