var password1 = document.getElementById('password');
var password2 = document.getElementById('confirm');

alert ("bite");
var checkPasswordValidity = function() {
    if (password1.value != password2.value) {
        password1.setCustomValidity('Les mots de passe doivent être identiques.');
    } else {
        password1.setCustomValidity('');
    }        
};

password1.addEventListener('change', checkPasswordValidity, false);
password2.addEventListener('change', checkPasswordValidity, false);

var form = document.getElementById('form');
form.addEventListener('submit', function() {
    checkPasswordValidity();
    if (!this.checkValidity()) {
        event.preventDefault();
        // Ajoutez ici la gestion de vos messages d'erreur.
        password1.focus();
    }
}, false);