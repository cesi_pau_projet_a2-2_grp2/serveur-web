var namefield = document.getElementById("product");
var catselect = document.getElementById("category");
var pricefield = document.getElementById("price");
var stockfield = document.getElementById("stock");
var form = document.getElementById("form");

namefield.onchange = () => verifyName();
catselect.onchange = () => verifyCat();
pricefield.onchange = () => verifyPrice();
stockfield.onchange = () => verifyStock();

function verifyName() { //Verify if the name is not empty and filled with expected value
	if (!namefield.value || namefield.value == "") {
		namefield.classList.add("errored-field");
		return false;
	} else {
		namefield.classList.remove("errored-field");
		return true;
	}
}

function verifyCat() { //Verify if the category is not empty and filled with expected value
	if (!catselect.value || catselect.value == 0) {
		catselect.classList.add("errored-field");
		return false;
	} else {
		catselect.classList.remove("errored-field");
		return true;
	}
}

function verifyPrice() { //Verify if the price is not empty and filled with expected value
	if (!pricefield.value || pricefield.value == 0) {
		pricefield.classList.add("errored-field");
		return false;
	} else {
		pricefield.classList.remove("errored-field");
		return true;
	}
}

function verifyStock() { //Verify if the price is not empty and filled with expected value
	if (!stockfield.value) {
		stockfield.classList.add("errored-field");
		return false;
	} else {
		stockfield.classList.remove("errored-field");
		return true;
	}
}


function verifyAll() { //Verify if the form is not empty and filled with expected value
	verifyName();
	verifyCat();
	verifyPrice();
	verifyStock();
	if (verifyName() && verifyCat() && verifyPrice() && verifyStock())
		form.submit();
}