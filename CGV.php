<?php
$title = "Conditions Générales de Vente";
include ("./layout/header_nav.php");
?>
    <main>
        <h1>CONDITIONS GÉNÉRALES DE VENTE</h1>
        <p>Les présentes conditions générales régissent la vente des produits présentés sur le site www.bdecesi.fr<br/>
            Toute commande suppose l'adhésion aux présentes conditions générales de vente. Le contrat de vente est soumis à la législation française.</p>
        
        <h2>1. L'OFFRE</h2>
        <p>Nos offres de produits, de services et modalités de livraison, aux prix indiqués en Euros, sont valables quelque soit la destination 
            en France.<br/>
            Toute commande vaut acceptation des prix et description des produits disponibles à la vente. Toutefois, une légère différence pourra apparaître
             entre les couleurs, dessins et écritures présentées sur le site www.bdecesi.fr.</p>
        
        <h2>2. LES PRIX</h2>
        <p>Nos prix sont indiqués en euros et tiennent compte de la TVA française en vigueur. Tout changement de taux de TVA pourrait être immédiatement 
            répercuté sur nos prix.<br/>
            Ces prix s'entendent avec remise en main propre lors d'un rendez-vous qui sera fixé par mail entre les membres du BDE, responsables de 
            la boutique, et les clients.</p>
        
        <h2>3. LA COMMANDE</h2>
        <p>La commande ne peut être enregistrée sur le site que si vous êtes clairement identifiés en remplissant le formulaire d'inscription en ligne. 
            De plus, une validation du panier ne pourra se faire sauf si les conditions précédemment énoncées sont remplies.<br/>
            Après la validation de votre commande, vous recevrez un e-mail d'un des membres du BDE vous confirmant la prise en compte de votre commande 
            ainsi qu'une proposition de créneau pour la remise en main propre de la commande.<br/>
            Pour tout renseignement complémentaire quant à la commande d'un ou de plusieurs produits, vous pouvez contacter les membres de BDE à l'adresse 
            e-mail BoutiqueBDE-CESI@cesi.fr</p>

        <h2>4. ACCEPTATION DE L'OFFRE ET CONFIRMATION DE LA COMMANDE</h2>
        <p>Le BDE du CESI vous confirme votre commande par réception d'un mail d'un membre du BDE. Il est conseillé de garder ce mail pour pouvoir récupérer 
            votre commande.<br/>
            Le BDE du CESI archive les commandes sur support informatique. Les clients peuvent y avoir accès sur simple demande écrite formulée auprès 
            du BDE du CESI.<br/>
            Le contrat est formé lors de la réception de la confirmation de commande par le client, ou lors de la réception du paiement de votre commande 
            par le BDE du CESI en cas de paiement via PayPal.</p>
        
        <h2>5. LE PAIEMENT</h2>
        <ul>
            <li>Par carte bancaire</li>
        </ul>
        <p>Le paiement internet des produits du BDE du CESI est sécurisé par l'utilisation de la solution de e-transaction PayPal.<br/>
            Lors de la validation de votre commande, connectez-vous avec votre compte PayPal afin de pouvoir initier et confirmer la transaction. 
            Différents accès à des serveurs d'autorisation sont effectués afin de vérifier les données pour
            éviter les abus et les fraudes. Pour cela, le serveur est en mode crypté et toutes les informations véhiculées sont codées (protocole). Rien 
            ne transite en clair sur Internet.<br/>
            Conformément à la Loi du 13 Mars 2000 sur la signature électronique, la transmission en ligne via PayPal de votre numéro de carte et la 
            validation finale de votre commande valent pour preuve de l'intégralité de la commande et de l'exigibilité des sommes dues en 
            règlement de cette commande.</p>
        <ul>
            <li>Paiement en espèces</li>
        </ul>
        <p>Le paiement en espèces des produits achetés sur la e-boutique du BDE du CESI se fait lors d'un rendez-vous obtenu avec un des membres du BDE 
            du CESI après réception de la confirmation de commande.<br/>
            Aucun paiement en espèces avant le rendez-vous programmé ne pourra être accepté. Afin d'éviter les abus et les fraudes, le paiement en espèces 
            se fera par remise en main propre du total dû par le client lors de la remise des produits commandés.</p>
        
        <h2>6. LA LIVRAISON</h2>
        <p>Le BDE du CESI n'effectue aucune livraison à domicile ou en centre CESI. Chaque commande sera remise en main propre au client lors d'un 
            rendez-vous fixé, après confirmation de commande, avec un membre du BDE.<br/>
            Pour les commandes "urgentes", nous vous conseillons de contacter les membres de votre BDE dès la fin de votre commande afin que celle-ci
            soit traitée de manière prioritaire. Attention, une raison valable sera demandée pour toute commande "urgente" afin d'éviter les abus.</p>

        <h2>7. RETOUR DES PRODUITS</h2>
        <p>Tout produit défectueux doit nous être retourné dans les 7 jours suivant sa réception, la remise en main propre valant réception. Le BDE du
            CESI procèdera alors à sont remplacement immédiat si sa responsabilité est avérée.<br/>
            Le client bénéficie du délai légal de rétractation de 14 jours à partir de la confirmation de commande pendant lequel il peut renoncer à sa 
            commande. La rétractation du client se fera par envoi d'un e-mail d'information aux membres du BDE, qui retourneront un formulaire de 
            rétractation. Ce dernier doit être dûment complété et signé par le client, avant d'être remis en main propre auprès d'un membre du BDE, le 
            14ème jour après la confirmation de commande valant date limite de rétractation.</p>

        <h2>8. DONNÉES NOMINATIVES</h2>
        <p>La collecte de vos données nominatives nous est nécessaire à la prise en compte de votre demande. En nous indiquant votre e-mail, vous recevrez
            une confirmation de votre commande, mais aussi des informations sur les différents événements proposés par les Bureaux des Étudiants du CESI.<br/>
            Les données nominatives ont été déclarées auprès de la CNIL sous le numéro 9281021.<br/>
            Conformément à la loi Informatique et Libertés du 6 Janvier 1978, vous disposez d'un droit d'accès, de modification, de rectification et de 
            suppression des données personnelles vous concernant. Si vous souhaitez exercer ce droit, il vous suffit de nous écrire en nous indiquant vos 
            nom, prénom, centre CESI et adresse e-mail.</p>

        <h2>9. PROPRIÉTÉ INTELLECTUELLE</h2>
        <p>La marque BDE CESI est déposée et les produits du BDE du CESI sont totale propriété de la marque.<br/>
            Il est possible de créer un lien vers la page de présentation de ce site sans autorisation expresse du BDE du CESI. Aucune autorisation ou 
            demande d'information préalable ne peut être exigée par le BDE du CESI à l'égard d'un site qui souhaite établir un lien vers le site 
            www.bdecesi.fr. Il convient toutefois d'afficher ce site dans une nouvelle fenêtre du navigateur. Il ne s'agira pas dans ce cas d'une 
            convnetion implicite d'affiliation.<br/>
            Dans tous les cas, tout lien, même tacitement autorisé, devra être retiré sur simple demande du BDE du CESI.</p>

        <h2>10. INTÉGRALITÉ</h2>
        <p>Dans l'hypothèse où l'une des clauses du présent contrat serait nulle et non avenue par une changement de législation, de règlementation ou 
            par une décision de justice, cela ne saurait en aucun cas affecter la validité et le respect des présentes Conditions générales de Vente.</p> 
        
        <h2>11. DURÉE</h2>
        <p>Les présentes conditions s'appliquent pendant toute la durée de mise en ligne des produits du BDE du CESI.</p>
        
        <h2>12. RESPONSABILITÉS</h2>
        <p>Le BDE du CESI ne pourrait voir sa responsabilité engagée pour les inconvénients et dommages relatifs à l'utilisation du réseau Internet tels 
            notamment une rupture dans le service, la présence de virus informatiques ou intrusions extérieures ou plus généralement tous cas qualifiés 
            de force majeure par les tribunaux.</p> 

        <h2>13. RÈGLEMENT DES LITIGES</h2>
        <p>En cas de litige, une solution amiable sera recherchée avant toute action judiciaire. À défaut, toute action judiciare sera portée devant les 
            tribunaux compétents.<br/>
            Les présentes conditions sont soumises à la loi française.</p>

    </main>
    
<?php include("./layout/footer.php")?>