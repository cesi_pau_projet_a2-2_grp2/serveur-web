<?php
$title = "Catalogue de produits";
include ("./layout/header_nav.php");
?>

    <main>
    <a href="homeShop.php"><input type="submit" value="◀">Accueil de la boutique</a>
        <aside>
            <?php if (DAO::$perm_level>0) {?> <!-- Allowed for all known users -->
                <a href="cart.php"><button><i class="fas fa-shopping-cart"></i>Mon panier</button></a><br/>
            <?php } ?>
            <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?> <!-- Allow BDE members and admins to add a product, add or delete a category. -->
            <a href="addProduct.php">Ajouter un produit</a>
            <form id="category_form" method="POST", action="./addCategoryRedirect.php"> <!-- Add a category in the database -->
            <input type="text" id="category" name="category" placeholder="Ajouter une catégorie..."><input type="submit" value="Ajouter"/>
            </form>
            <form id="delete_category_form" method="POST", action="./deleteCategoryRedirect.php">
            <select name="deleting_category">
                <option disabled selected>Toutes catégories</option>
                <?php foreach (DAO::get_category() as $category) { ?>
                <option value="<?= $category->id_category ?>"><?= $category->category_name ?></option> <!-- Shows each category that is in the database -->
                <?php } ?>
            </select>
            <input type="submit" value="Supprimer la catégorie sélectionnée"/> <!-- Delete a category from the database -->
            <?php } ?>
            </form>
        </aside> <!-- We allow the users and visitors to filter or search some products -->
        <input id="txtSearch" type="search" placeholder="Rechercher..."><input type="submit" value="🔎"><br/>
        <input type="checkbox" id="price_LOEthan"><label for="price_LOEthan">Prix inférieur ou égal à</label><input id="max" type="number"><br/>
        <input type="checkbox" id="price_GOEthan"><label for="price_GOEthan">Prix supérieur ou égal à</label><input id="min" type="number">
        <aside>
            <label>Filtres</label><br/>
            <select>
                <option selected>Toutes catégories</option>
                <?php foreach (DAO::get_category() as $category) { ?> <!-- Shows each category that is in the database -->
                <option><?= $category->category_name ?></option>
                <?php } ?>
            </select>
        </aside>
        
        <table id="table" class="display" style="width:100%"><thead>
        <tr>
        <th>Nom</th>
        <th>Description</th>
        <th>Prix</th>
        <th>Stock</th>
        <th>Catégorie</th>
        </tr>
        </thead></table>

        <div class="row">
            <!-- <?php var_dump(DAO::get_products()); ?>-->
            <?php foreach (DAO::get_products() as $product) { ?> <!-- Here, we show the products from the database -->
                <div class="col">
                    <label><?= $product->product_name ?></label><br/>
                    <a href="./picturedetails.php?1"><img src="./assets/test.png" /></a><br/>
                    <label><?= $product->product_desc ?></label><br/>
                    <label><?= $product->product_price ?></label><br/>
                    <label><?= $product->product_stock ?></label><br/>
                    <label><?= $product->id_category ?></label><br/>
                    <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?> <!-- Allow BDE members and admins to delete the product -->
                    <a href="deleteProductRedirect"><button>Supprimer le produit</button></a><br/>
                    <?php } ?>
                    <?php if (DAO::$perm_level == 0) {?> <!-- If a visitor wants to add a product to the cart, we invite him to sign up -->
                    <a href="Sign%20up.php"><button>AJOUTER AU PANIER</button></a>
                    <?php } else { ?>
                    <button>AJOUTER AU PANIER</button> <!-- Else, we allow the adding of the product to the cart -->
                    <?php } ?>
                </div>
                <?php } ?>            
            </div>
    </main>

<?php include("./layout/footer.php")?>
        <script>
            // Filters by searching data in column 3 between 2 values
            $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    var min = parseFloat( $('#min').val());
                    var max = parseFloat( $('#max').val());
                    var price = parseFloat( data[2] ) || 0; // use data for the price column
 
        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && price <= max ) ||
             ( min <= price   && isNaN( max ) ) ||
             ( min <= price   && price <= max ) )
        {
            return true;
        }
        return false;
    }
);

            $(document).ready(function(){

             // DataTable
            var table = $('#table').DataTable({
                "ajax": {
                    "url": 'http://10.64.128.111:8001/products',
                    "dataSrc": ""
                },
                "columns": [
                    { "data": 'product_name' , "searchable": true },
                    { "data": 'product_desc' , "searchable": true },
                    { "data": 'product_price', "searchable": true },
                    { "data": 'product_stock', "searchable": true },
                    { "data": 'category_name', "searchable": true }
                ]
                
            });

            // Event listener to the two range filtering inputs to redraw on input
            $('#min, #max').keyup( function() {
                table.draw();
            });

            // The Search
            table.columns().every(function(){
                var that = this;

                $('input', this.footer()).on('keyup change', function(){
                    if (that.search() !== this.value){
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        });
        </script>