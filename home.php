<?php 
$title = "BDE CESI";
//$nom = "Jean-Michel Générique";
include("./layout/header_nav.php");
if (isset($_GET ["consent"])) { // If the visitor has clicked on OK button
    setcookie("bdeconsent", 1, time()+86400*365); //Cookie installed for a year
}
$consent = isset($_COOKIE["bdeconsent"]); //We set the cookie to 1, to proove that the cookie has been installed on the visitor's computer.
?>

<section>
    <h1>Bienvenue sur le site des Bureaux des Étudiants des centres CESI</h1><br/>
    <h2>Voici les événements les plus populaires des BDE des centres CESI</h2>
</section>

<article>
    <h3>CESI Cup</h3>
    <img src="./assets/test.png"/>
    <p>La CESI Cup regroupe les différentes branches d'un même centre CESI autour de différentes activités :</p>
    <ul>
        <li>Activités sportives</li>
        <li>Jeux en équipes</li>
        <li>Parcours du combattant</li>
        <li>Quiz de culture générale</li>
        <li>Épreuves de dégustation</li>
        <li>Matches de logique</li>
        <li>Et autres activités...</li>
    </ul>
    <p>Toutes les équipes s'affronteront lors d'une demi-journée avec à la clé pour l'équipe vainqueure, une montagne de cadeaux !</p>
    <p>La CESI Cup est organisée par le BDE de chaque centre CESI, et la participation des étudiants est gratuite et facultative.</p>
</article>
<article>
    <h3>Césiades</h3>
    <img src="./assets/test.png"/>
    <p>Les Césiades sont une version un peu plus nationale de la CESI Cup, où tous les centres de France s'affrontent autour d'épreuves sportives :</p>
    <ul>
        <li>Match de football</li>
        <li>Concours de pétanque</li>
        <li>Tir à l'arc</li>
        <li>Match de basketball</li>
        <li>Match de handball</li>
        <li>Tir à la corde</li>
        <li>Sprint sur 100 m</li>
        <li>Et bien d'autres épreuves...</li>
    </ul>
    <p>Chaque centre désigne une équipe de 15 étudiants qui se rendent dans la ville du centre CESI vainqueur en titre afin de s'affronter dans des 
        épreuves toutes plus physiques les unes que les autres.</p>
    <p>Les Césiades sont financées par les BDE de tous les centres CESI. Tous les étudiants peuvent s'inscrire dans la liste qui représentera leur centre.
        Les inscrits et sélectionnés voient leur participation devenue obligatoire.</p>
</article>
<article>
    <h3>Week-end d'intégration</h3>
    <img src="./assets/test.png"/>
    <p>Le week-end d'intégration permet aux étudiants de première année de passer un weekend afin de rencontrer les autres étudiants d'un centre CESI.
        Au cours de ce week-end sont prévus :</p>
    <ul>
        <li>Activités de présentation</li>
        <li>Activités sportives</li>
        <li>Jeux en tout genre</li>
        <li>Activités aquatiques si beau temps</li>
        <li>Activités prévues si mauvais temps</li>
        <li>Déjeuners, dîner et petit-déjeuner inclus</li>
        <li>Soirée musique avec les autres étudiants</li>
    </ul>
    <p>L'organisation des week-end d'intégration est à la charge entière des BDE des centres CESI. La participation de tous les étudiants est 
        facultative, y compris pour les étudiants de première année. Les frais de participation varieront suivant les BDE des centres CESI.</p>
</article>
<?php if ($consent == false) { ?> <!-- The banner is shown if the cookie is not installed on the visitor's computer. If so, the banner is hidden. -->

<div id="cookie-notice" role="banner" class="cn-bottom" style="color: white; background-color: black; visibility: visible;">
    <div class="cookie-notice-container">
        <span id="cn-notice-text">
            En utilisant ce site, vous acceptez l'utilisation de cookies pour améliorer votre navigation
        </span>
        <a href="./home.php?consent=1" id="cn-accept-cookies" data-cookie-set="accept" class="cn-set-cookie cn-button button"><button>   OK   </button></a>
        <a href="./PrivacyPolicy.php" target="_blank" id="cn-more-info" class="cn-more-info cn-button button"><button>   En savoir plus   </button></a>
    </div>
</div>
<?php } ?>
<?php include("./layout/footer.php") ?>