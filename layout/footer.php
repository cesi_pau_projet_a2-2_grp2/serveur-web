</main>
    </div>
    <footer id="bottom_bar">
        <nav class="row">
            <div class="col-lg">
                <a href="PrivacyPolicy.php">Politique de confidentialité</a>
            </div>
            <div class="col-lg">
                <a href="LegalNotice.php">Mentions légales</a>
            </div>
            <div class="col-lg">
                <a href="CGV.php">Conditions Générales de Vente</a>
            </div>
            <div class="col-lg">
                <span><i class="far fa-copyright"></i>Projet-2 2019</span>
            </div>
        </nav>
    </footer>
</body>
<script src="./assets/jquery/jquery-3.3.1.min.js"></script>
<script src="./assets/bootstrap/js/bootstrap.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
</html>