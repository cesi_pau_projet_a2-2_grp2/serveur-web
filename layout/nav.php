<aside class="col-md-2" id="navbar">
<?php if (DAO::$perm_level > 0) {?>
	<a href="userHome.php"><img id="logo" src="https://ecole-ingenieurs.cesi.fr/wp-content/uploads/sites/5/2018/10/logo-cesi-ecole-ingenieurs.png"></a>
<?php } else {?>
	<a href="home.php"><img id="logo" src="https://ecole-ingenieurs.cesi.fr/wp-content/uploads/sites/5/2018/10/logo-cesi-ecole-ingenieurs.png"></a>
<?php } ?>
	<nav id="navbar_links">
		<a href="./ideasbox.php">BOÎTE À IDÉES</a>
		<a href="./EventsList.php">MANIFESTATIONS</a>
		<a href="./homeShop.php">BOUTIQUE</a>
		<?php if (DAO::$perm_level == 3 || DAO::$perm_level == 4) {?>
		<a href="Download.php">TÉLÉCHARGER LES PHOTOS</a>
		<?php } ?>
	</nav>
	Retrouvez-nous sur&nbsp;:
	<nav id="navbar_social">
		<a href="https://www.facebook.com/CESIingenieurs/" target="_BLANK"><i class="fab fa-facebook-square"></i></a>
		<a href="https://twitter.com/GroupeCESI" target="_BLANK"><i class="fab fa-twitter-square"></i></a>
		<a href="https://www.instagram.com/campus_cesi/" target="_BLANK"><i class="fab fa-instagram"></i></a>
		<a href="https://www.linkedin.com/company/groupe-cesi/" target="_BLANK"><i class="fab fa-linkedin"></i></a>
	</nav>
</aside>