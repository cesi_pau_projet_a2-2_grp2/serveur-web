<header>
	<div class="row" id="top_bar">
		<div class="col">
			<h1><?= isset($title) ? mb_strtoupper($title) : '<span style="color : red">GUILLAUME, LE TITRE STP</span>' ?></h1>
		</div>
		<?php if (!DAO::$perm_level) { ?>
			<nav class="col-md-auto">
				<a href="./Sign%20in.php">SE CONNECTER</a>
				<span class="vrt_separator"></span>
				<a href="./Sign%20up.php">S'INSCRIRE</a>
			</nav>
		<?php } else { ?>
			<nav class="col-md-auto">
				<a href="./LogoutRedirect.php">DÉCONNEXION</a>
				<span class="vrt_separator"></span>
				<a href="./userSettings.php">MON COMPTE</a>
				<span class="vrt_separator"></span>
				<?= mb_strtoupper(DAO::$name)." ".mb_strtoupper(DAO::$surname) ?>
			</nav>
		<?php } ?>
	</div>
</header>