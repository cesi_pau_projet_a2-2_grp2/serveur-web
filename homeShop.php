<?php
$title = "Boutique du BDE";
include ("./layout/header_nav.php");
?>

    <main>
        <h1>Bienvenue dans la boutique<?= DAO::$perm_level>0 ? ", ".DAO::$name : ""?></h1>
        <!-- This line put form the database the name of the owner of the account using its  id. Then, it shows the right name on the Web page. -->
        <?php if (DAO::$perm_level>0) {?> <!-- Allow all known users to go to their cart, by the way all known users can add a product to the cart -->
        <aside>
            <a href="cart.php"><button><i class="fas fa-shopping-cart"></i>Mon panier</button></a>
        </aside>
        <?php } ?>
        <input type="search" placeholder="Rechercher"><input type="submit" value="🔎">

        <h2>Les 3 articles les plus vendus en ce moment : </h2>
        <div class="row">
            <div class="col-lg">
                <img src="./assets/back.jpg">
            </div>
            <div class="col-lg">
                <img src="./assets/slide2.jpg">
            </div>
            <div class="col-lg">
                <img src="./assets/slide3.jpg">
            </div>
        </div>
        <h2><a href="products.php">Accéder au catalogue</a></h2>
        <h3>À propos de nous</h3>
        <h4>La présentation du BDE</h4>
        <p>Nous sommes un groupe d'élèves qui avons pour but de créer, organiser et proposer tout un tas de manifestations destinées aux étudiants
            afin de leur permettre de s'amuser avec d'autres étudiants, en dehors du CESI, et que cela coûte le moins cher possible.</p>
        <p>Le CESI finance une partie du coût de l'organisation des manifestations, et le BDE peut en assumer une petite partie. La création de 
            cette boutique vient compléter le financement qui peut être manquant pour certaines manifestations, afin de pouvoir vous proposer 
            toujours mieux, toujours plus souvent, toujours moins cher.</p>
        <h4>Ce à quoi vous partcipez avec vos achats</h4>
        <p>Avec vos achats, vous investissez dans votre BDE et vous permettez : <p>
        <ul>
            <li>Le financement de manifestations futures</li>
            <li>La promotion de l'école dès que vous montrez vos achats</li>
            <li>Un nombre de manifestations gratuites toujours plus important</li>
            <li>Plus généralement, des manifestations toujours plus nombreuses !</li>
        </ul>
        <h4>Ce qui pourrait nous être super utile ;)</h4>
        <p>Afin de pouvoir augmenter nos ventes et obtenir des financements, nous vous invitons à :</p>
        <ul>
            <li>Partager le lien de la boutique à toutes vos connaissances</li>
            <li>Faire la promo des articles que nous vendons</li>
            <li>Si vous avez des idées de nouveaux produits à suggérer, contactez les membres de votre BDE</li>
            <li>N'hésitez surtout pas à promouvoir la boutique auprès de tous les membres du CESI pour aider notre développement ! :)</li>
        </ul>

    </main>

<?php include("./layout/footer.php")?>