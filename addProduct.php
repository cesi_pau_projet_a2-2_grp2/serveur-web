<?php
$title = "Ajout de Produit";
include ("./layout/header_nav.php");
?>

    <main>
        <form id="form" method="POST", action="./addProductRedirect.php">
            <a href="products.php"><input type="submit" value="◀">Retour au catalogue</a><br/><!-- If the user wants to return to the products list, we allow him to do it -->
            <label for="product">Nom du produit à ajouter</label><br/><!-- Add a name for the future product that will be added in the products list -->
            <input type="text" id="product" name="product" placeholder="Nom du produit"></p>
            <label for="product_desc">Description du produit</label><br/><!-- Add a description for the future product that will be added in the products list -->
            <textarea id="product_desc" name="product_desc" rows="5" cols="60" placeholder="Description..."></textarea></p>
            <label for="category">Catégorie</label><br/><!-- Add a category for the future product that will be added in the products list -->
            <select id="category" name="category">
            <option disabled selected>Choisir une catégorie...</option>
                <?php foreach (DAO::get_category() as $category) { ?> <!-- Shows each category that is in the database -->
                <option value="<?= $category->id_category ?>"><?= $category->category_name ?></option>
                <?php } ?>
            </select><br/>
            <label for="price">Prix de vente</label><!-- Add a price for the future product that will be added in the products list -->
            <p><input type="number" id="price" name="price" step="0.01" min="0">€</p>
            <label for="price">Quantité en stock</label><!-- Add a stock quantity for the future product that will be added in the products list -->
            <p><input type="number" id="stock" name="stock" step="1" min="1"></p>
            <label for="addPic">Ajouter une photo du produit</label><br/><!-- Add a picture for the future product that will be added in the products list -->
            <input type="file" id="addPic" value="Ajouter une photo..."><br/>
        </form>
            <p>
                <button onclick="verifyAll();">Ajouter le produit</button>
                <!-- Call the verifyAll() JavaScript function in order to verify if the form is filled with the expected values and not empty -->
            </p>
        
    </main>
<script src="./assets/verifyProduct.js"></script>
<?php include("./layout/footer.php")?>