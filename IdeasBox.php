<?php
header("Access-Control-Allow-Origin: *");
$title = "Boîte à idées";
include ("./layout/header_nav.php");
?>

    <main>
        <?php if (DAO::$perm_level>0) {?> <!-- Allow all known users to suggest an idea for a future event -->
        <section>
        <form id="idea_form" method="POST" action="./IdeasBoxRedirect.php">
            <label for="idea">Suggérez-nous une idée</label>
            <p><input type="text" name="idea" id="idea" placeholder="Description"><input type="submit" value="Soumettre l'idée"></p>
            </form>
        </section>
        <?php } ?>

        <section>
        <?php foreach (DAO::get_ideas() as $idea) { ?>
        <div>
            <h4><?= $idea->idea_desc ?></h4>
            <aside>
                <?php if (DAO::$perm_level>0) {?>  
                    <button onclick='like(<?=$idea->id_idea?>, "<?=$_COOKIE["BDE_token"]?>");' id="like<?=$idea->id_idea?>"><i class="far fa-thumbs-up"></i><span id="likeindicator<?=$idea->id_idea?>">BONNE IDÉE</span></button><span id="counter<?=$idea->id_idea?>"><?= -1//$idea->upvotes ?></span><br/>
                <?php } else {?>
                    <i class="far fa-thumbs-up"></i><?= -1//$idea->idea_upvotes ?><br/>
                <?php } ?>
                <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?> <!-- Allow only BDE members and admins to add an idea as an event -->
                <a href="./Create-EditEvents.php"><button>Ajouter l'idée en tant que manifestation</button></a>
                <button><i class="fas fa-times"></i></button><br/> <!-- But also to delete the idea -->
                <?php } ?>
                <?php if (DAO::$perm_level >= 3) {?> <!-- Allow CESI's employees and admins to report the idea -->
                <button><a href="report.php"><i class="fas fa-flag"></i>Signaler</a></button>
                <?php } ?>
            </aside>
        </div>
        <?php } ?>
        </section>
    </main>

<script src="./assets/ideasBox.js"></script>
<?php include("./layout/footer.php")?>