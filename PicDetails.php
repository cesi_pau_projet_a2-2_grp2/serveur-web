<?php
$title = "Détails de la photo";
include ("./layout/header_nav.php");
?>
        
    <main>
        <a href="EventsDetails.php"><input type="submit" value="◀">Vue d'ensemble</a>
        <aside>
            <h2>Soirée bowling</h2>
            <img src="./assets/test.png"/>
            <p>Publiée par Jean-Michel Générique</p>
            <?php if (DAO::$perm_level >= 3) {?> <!-- Allow CESI's employees and admins to report the picture -->
            <a href="report.php"><button>SIGNALER</button></a><br/>
            <?php } ?>
            <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?> <!-- Allow BDE members and admins to delete the picture -->
            <button>SUPPRIMER</button><br/>
            <?php } ?>
            <?php if (DAO::$perm_level>0) {?> <!-- Allow all known users to like the picture -->
            <button>J'AIME</button><br/><br/>
            <?php } ?>
            32<i class="far fa-thumbs-up"></i>
        </aside>

        <h2>Commentaires</h2>
        <ul>
            Alice Dujardin : Super photo !!!
            <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?> <!-- Allow BDE members and admins to delete the comment-->
            <button><i class="fas fa-times"></i>Supprimer</button>
            <?php } ?>
            <?php if (DAO::$perm_level >= 3) {?> <!--Allow CESI's employees and admins to report the comment -->
            <button><a href="report.php"><i class="fas fa-flag"></i>Signaler</a></button><br/>
            <?php } ?><br/>
            Bob Tout-le-monde : PTDRRRRR Charlie
            <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?>
            <button><i class="fas fa-times"></i>Supprimer</button>
            <?php } ?>
            <?php if (DAO::$perm_level >= 3) {?>
            <button><a href="report.php"><i class="fas fa-flag"></i>Signaler</a></button><br/>
            <?php } ?><br/>
            Charlie Jambon : On en parle de ta tête Bob ?
            <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?>
            <button><i class="fas fa-times"></i>Supprimer</button>
            <?php } ?>
            <?php if (DAO::$perm_level >= 3) {?>
            <button><a href="report.php"><i class="fas fa-flag"></i>Signaler</a></button><br/>
            <?php } ?>
        </ul>
        <?php if (DAO::$perm_level>0) {?> <!-- Allow all known users to post a comment -->
        <input type="text" placeholder="Ajouter un commentaire"><input type="submit" value="Poster">
        <?php } ?>

    </main>

<?php include("./layout/footer.php")?>