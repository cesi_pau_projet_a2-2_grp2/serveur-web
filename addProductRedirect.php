<?php
if (isset($_POST["product"],$_POST["category"], $_POST["price"], $_POST["stock"])) {
    /**
     * The POST parameter is put into a variable 
     */
    $product = $_POST["product"];
    $category = $_POST["category"];
    $price = $_POST["price"];
    $desc = $_POST["product_desc"];
    $stock = $_POST["stock"];

    require_once("data_access.php");

    
        
    $content = json_encode(["token" => $_COOKIE["BDE_token"], "name"=>$product, "desc"=>$desc, "price"=>$price, "categoryID"=>$category, "stock"=>$stock]);
/**
 * Translates the array into a JSON file which will be submitted to the API
 * Then add a product with its specifications to the database
 * Finally shows the right page to the user
 */
        EasyCURL::post("/product",$content);
        header("Location: ./products.php");
        
    
}
?>