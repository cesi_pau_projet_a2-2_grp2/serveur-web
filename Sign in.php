<?php 
$title = "Connexion";
include("./layout/header_no_nav.php") ;
?>
<div class="row" id="general_row">
    <section id="left_box" class="col-md">
        <div>
            <h1>Pas encore inscrit&nbsp;?</h1>
            <h2>En vous inscrivant, vous pourrez&nbsp;: </h2>
            <ul>
                <li>Suggérer des idées de manifestations</li>
                <li>Vous inscrire à celles-ci</li>
                <li>Poster, liker et commenter des photos</li>
                <li>Commander des goodies dans notre boutique</li>
            </ul>
            <nav>
                <a href="Sign%20up.php"><button>INSCRIVEZ-VOUS</button></a>
            </nav>
        </div>
    </section>
    
    <section id="right_box" class="col-md">
    <form action="./SignInRedirect.php" method="POST" id="InForm" novalidate onsubmit="return validateInForm()">
        <h1>Connectez-vous</h1>
        <label for="email">Adresse e-mail</label>
        <p><input type="email" id="email" name="email" placeholder="Votre adresse e-mail"/></p>
        <label for="email">Mot de passe</label>
        <p><input type="password" id="password" name="password" placeholder="Votre mot de passe"/></p>

        <nav>
            <input type="submit" value="CONNECTEZ-VOUS"/>
        </nav>

        <p>Mot de passe oublié&nbsp;?<br/>
        Contactez un administrateur</p>
    </form>
    </section>
</div>
<script src="./assets/VerifySigningIn.js"></script>
<?php include("./layout/footer.php") ?>