<?php
$title = "Politique de Confidentialité";
include ("./layout/header_nav.php");
?>

    <main>
        <header>
            Les Bureaux des Étudiants des centres CESI sont engagés dans une démarche continue de conformité avec le Règlement Général sur la Protection 
            des Données du 25 Mai 2018. Avec ce nouveau règlement, les BDE des centres CESI renforcent leur politique de protection des données personnelles 
            afin que les données de nos utilisateurs soient collectées et utilisées de manière transparente, confidentielle est sécurisée.
        </header>

        <h1>Politique de protection des données personnelles à partir du 25 Mai 2018</h1>
        <p>Notre politique de protection des données personnelles décrit la manière dont les BDE CESI traitent les données à caractère personnel des 
            visiteurs et des utilisateurs (ci-après les "Utilisateurs") lors de leur navigation sur notre site www.bdecesi.fr<br/>
            La Politique de Protection des Données Personnelles fait partie intégrante des Mentions Légales du site.<br/>
            Les BDE CESI accordent en permanence une attention aux données de nos Utilisateurs. Nous pouvons ainsi être amenés à modifier, compléter 
            ou mettre à jour la Politique de Protection de Données Personnelles. Nous vous invitions à conulter régulièrement la dernière version 
            en vigueur, accessible sur notre site.<br/>
            Si des modifications majeures sont apportées, nous vous informerons par e-mail ou par nos services pour vous permettre d'examiner ces 
            modifications avant qu'elles ne prennent effet.<br/>
            Si vous continuez à utiliser nos Services après la publication ou l'envoi d'un avis concernant les modifications apportées à la Politique de 
            Protection des Données Personnelles, cela signifie que vous acceptez les mises à jour.</p>

        <h2>Données personnelles et finalités</h2>
        <p>Par donnée à caractère personnel, on entend toute information se rapportant à une personne identifiée ou identifiable, notamment par 
            référence à des identifiants tels qu'un nom, un numéro d'identification, des données de localisation, un identifiant en ligne, un ou 
            plusieurs éléments spécifiques propres à l'identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale, 
            ainsi qu'à tout autre renseignement que nos clients décident de nous communiquer.<br/>
            Lorsque vous utilisez notre site et que vous souhaitez recevoir des informations sur nos offres, via un formulaire de contact ou bien 
            un formulaire de candidature, nous collections et traitons des données à caractère personnel vous concernant telles que : votre Civilité, 
            Nom, Prénom, Adresse e-mail, Centre CESI.<br/></p>

        <h2>Cookies</h2>
        <p>Nous utilisons des cookies sur notre site pour les besoins de votre navigation, l'optimisation et la personnalisation de nos Services 
            sur notre plateforme en mémorisant vos préférences.<br/>
            Vous pouvez à tout moment désactiver les cookies<br/>
            Définition de "cookie" et son utilisation. Un "cookie" est un fichier texte déposé sur votre ordinateur lors de la visite de notre plateforme.
            Dans votre ordinateur, les cookies sont gérés par votre navigateur Internet.<br/>
            Nous utilisons des cookies sur notre site pour les besoins de votre navigation, l'optimisation et la personnalisation de nos Services sur 
            notre plateforme en mémorisant vos préférences. Les cookies nous permettent aussi de voit comment notre plateforme est utilisée. Nous 
            recueillons automatiquement votre adresse IP et des informations relatives à l'utilisation de notre site. Notre platemforme peut ainsi se 
            souvenir de votre identité lorsqu'une connexion a été établie entre le serveur et le navigateur Web. Les informations fournies 
            précédemment dans un formulaire Web pourront ainsi être conservées.<br/>

            Différents types de cookies sont utilisés sur notre site :</p>
            <ul>
                <li>Des cookies sont strictement nécessaires au fonctionnement de notre plateforme. Ils vous permettent d'utiliser les principales 
                    fonctionnalités de notre plateforme (par exemple l'accès à votre compte). Sans ces cookies, vous ne pourrez pas utiliser notre 
                    plateforme normalement.</li>
                <li>Des cookies dits "analytiques" : afin d'améliorer nos services, nous utilisons des cookies de mesure d'audience telles que le nombre 
                    de pages vues, le nombre de visites, l'activité des utilisateurs et leur fréquence de retour, notamment grâce aux services de 
                    Google Analytics. Ces cookies permettent seulement l'établissement d'études statistiques sur le trafic des utilisateurs sur notre 
                    plateforme, dont les résultats sont totalement anonymes pour nous permettre de connaître l'utilisation et les performances de notre 
                    plateforme et d'en améliorer le fonctionnement. Accepter ces cookies est une condition nécessaire à l'utilisation de notre plateforme. 
                    Si vous les refusez, nous ne pouvons vous garantir une utilisation normale sur notre plateforme.</li>
                <li>Des cookies fonctionnels : il s'agit des cookies qui nous permettent de personnaliser votre expérience sur notre plateforme en 
                    mémorisant vos préférences. Ces cookies peuvent être placés par une tierce partie pour notre compte, mais elle n'est pas autorisée 
                    à les utiliser à d'autres fins que celles décrites.</li>
                <li>Des cookies de ciblage : ces cookies sont liés aux services fournis par des tierces parties, comme le bouton "J'aime". Ces 
                    cookies sont placés par des tierces parties.</li>
            </ul>

            <p>Types de cookies utilisés. Les types de cookies suivants sont utilisés sur ce site :</p>
            <ul>
                <li>Cookies "temporaires" : ce type de cookie est actif dans votre navigateur jusqu'à ce que vous quittiez notre palteforme et expire si 
                    vous n'accédez pas au site pendant une certaine période donnée.</li>
                <li>Cookies "permanents" ou "traceurs" : ce type de cookie reste dans le fichier de cookies de votre navigateur pendant une période plus 
                    longue, qui dépend des paramètres de votre navigateur Web. Les cookies permanents sont également appelés cookies traceurs.</li>
            </ul>

            <p>Utilisation des cookies de tiers. Nous pouvons recourir à des partenaires tiers, tels que Google Analytics, pour suivre l'activité des 
                visiteurs de notre plateforme ou afin d'identifier vos centres d'intérêt sur notre plateforme et personnaliser l'offre qui vous est 
                adressée sur notre plateforme ou en dehors de notre plateforme. Les informations pouvant être ainsi collectées par des annonceurs tiers 
                peuvent inclure des données telles que des données de géolocalisation ou des informations de contact, comme des adresses électroniques. 
                Les politiques de confidentialité de ces annonceurs tiers fournissent des informations supplémentaires sur la manière dont les cookies 
                sont utilisés.</p>

            <p>Nous veillons à ce que les sociétés partenaires acceptent de traiter les informations collectées sur notre plateforme exclusivement pour nos 
                besoins et conformément à nos instructions, dans le respect de la réglementation Européenne et s'engagent à mettre en oeuvre des mesures 
                appropriées de sécurisation et de protection de la confidentialité des données.</p>

            <p>Désactivation des cookies. Vous pouvez à tout moment désactiver les cookies en sélectionnant les paramètres appropriés de votre navigateur 
                pour désactiver les cookies (la rubrique d'aide du navigateur utilisé précise la marche à suivre).</p>
            
            <p>Nous attirons votre attention sur le fait que la désactivation des cookies peut réduire ou empêcher l'accessibilité à tout ou partie de 
                certaines fonctions.</p>

            <p>En ce qui concerne la publicité sur des sites tiers, vous pouvez vou reporter à notre Politique relative aux Cookies pour comprendre comment 
                retirer votre consentement.</p>
            <p>Nous collectons les informations que vous nous fournissez notamment quand :</p>
            <ul>
                <li>Vous créez, modifiez et accédez à votre compte personnel</li>
                <li>Vous remplissez un formulaire</li>
                <li>Vous contactez notre Service Client</li>
            </ul>

            <h2>Partage avec des tiers</h2>
            <p>Nous encadronf juridiquement les transferts par des engagements contractuels avec nos sous-traitants pour vous apporter un niveau 
                élevé de sécurité.</p>
            
            <p>Les données personnelles vous concernant collectées sur notre site sont destinées pour la propre utilisation par le BDE du CESI et 
                peuvent être transmises aux sociétés sous-traitantes auxquelles les BDE du CESI peuvent faire appel dans le cadre de l'exécution de ses 
                services.</p>

            <p>Le BDE du CESI ne vend ni loue vos données personnelles à des tiers à des fins de marketing, en aucun cas.</p>

            <p>Nous travaillons également en étroite collaboration avec des entreprises tierces qui peuvent avoir accès à vos données personnelles, 
                en particulier :</p>
            <ul>
                <li>Lorsque nous faisons appel à des fournisseurs de moteurs de recherche et de solutions analytiques pour améliorer et optimiser 
                    notre plateforme ;</li>
                <li>Lorsque nous avons l'obligation légale de faire ou si nous pensons de bonne foi que cela est nécessaire pour (i) répondre à toute 
                    récalmation à l'encontre des BDE du CESI, (ii) se conformer à toute demande judiciaire, (iii) faire exécuter tout contrat conclu avec 
                    nos membres, tel que les Conditions Générales d'Utilisation et la présente Politique de Protection des Données Personnelles, (iv) en 
                    cas d'urgence mettant en jeu la santé publique ou l'intégrité physique d'une personne, (v) dans le cadre d'enquêtes et d'investigations 
                    ou (vi) afin de garantir les droits, les biens et la sécurité des BDE du CESI, ses membres et plus généralement tout tiers ;</li>
                <li>En outre, les BDE du CESI ne divulguent pas vos données personnelles à des tiers, excepté si (1) vous en formulez la demande ou autorisez 
                    la divulgation ; (2) la divulgation est requise pour traiter des transactions ou fournir des services que vous avez demandés ; (3) les 
                    BDE du CESI y sont contraints par une autorité gouvernementale ou un organisme de réglementation, en cas de réquisition judiciaire, de 
                    citation à comparaître ou de toute autre exigence gouvernementale ou judiciaire similaire, ou pour établir ou défendre une demande légale ; 
                    ou (4) le tiers agit en tant qu'agent ou de sous-traitant d'un des BDE du CESI dans l'exécution de ses services.</li>
            </ul>

            <p>Si les BDE du CESI ou tout ou partie de ses actifs sont rachetés par un tiers, les données en notre possession seront, le cas échéant, 
                transférées au nouveau propriétaire.</p>

            <p>Nous pouvons agréger des données qui vous concernent et que nous recevons ou envoyons à nos partnaires commerciaux, notamment tout ou 
                partie de vos données personnelles et les informations collectées par l'intermédiaire de cookies. Ces informations agrégées ne seront 
                utilisées que pour les finalités décrites ci-dessus.</p>

            <h2>Protection des données personnelles</h2>
            <p>Nous prenons toutes les dispositions nécessaires pour que vos données soient protégées. Nous vous demandons également de veiller à la 
                confidentialité des données.<br/>
                Les BDE du CESI appliquent les mesures de sécurité technologique et organisationnelles généralement reconnues afin que les données à 
                caractère personnel recueillies ne soient ni perdues, ni détournées, ni consultées, ni modifiées, ni divulguées par des tiers non 
                autorisés sauf si la communication de ces données est imposée par la réglementation en vigueur, notamment à la requête d'une autorité 
                judiciaire, de police, de gendarmerie ou de toute autre autorité habilitée par la loi.</p>

            <p>La sécurité des données personnelles dépend également des Utilisateurs. Les Utilisateurs qui sont membres des BDE du CESI s'engagent à 
                conserver la confidentialité de leur identifiant et de leur mot de passe. Les membres s'engagent également à ne pas partager leur compte 
                et à déclarer aux BDE du CESI toute utilisation non autorisée dudit compte dès lors qu'ils en ont connaissance.</p>

            <h2>Durée de conservation</h2>
            <p>Vos données sont conservées pour une durée limitée afin de vous assurer une sécurité optimale.<br/>
                Ces données seront conservées pendant toute la durée de votre inscription sur le site. En cas d'absence de commande passée depuis plus de 
                trois (3) ans, les données relatives au compte seront conservées pendant une durée de 15 ans à compter de la dernière date de connexion. 
                Elles font ensuite l'objet d'un archivage jusqu'à l'expiration de la durée de prescription légale applicable.<br/>
                En ce qui concerne les activités proposées, nous conservons vos données personnelles pour la plus longue des durées légales et 
                réglementaires applicables ou une autre durée compte tenu des contraintes opérationnelles tells qu'une gestion efficace de la relation 
                étudiants et les réponses aux demandes du Répertoire National des Certifications Professionnelles.<br/>
                Les données collectées à des fins de prospection commerciale sont conservées pendant une durée maximale de trois (3) ans à compter de la fin de 
                la relation commerciale.</p>

            <h2>Utilisateurs mineurs</h2>
            <p>Des mineurs, dans le cadre de leur recherche d'informations, peuvent accéder à notre site. Ils peuvent également demander des informations 
                complémentaires sur certains points.<br/>
                Néanmoins, toute inscription définitive du mineur, ne se fera qu'après consentement du ou des titulaires de l'autorité parentale. En tant 
                que mineur, vous pourrez exercer votre droit à l'oubli si vous ne souhaitez plus que les données vous concernant soient dans nos bases 
                de données.</p>

            <h2>Relation entre les BDE du CESI et les réseaux sociaux</h2>
            <p>Nous ne sommes pas responsables de l'utilisation des données faite par les réseaux sociaux.<br/>
                Vous pouvez les paramétrer sur leurs sites.<br/>
                Lorsque vous utilisez des réseaux sociaux et des services ou application des BDE du CESI en relation avec des réseaux sociaux, cela est 
                susceptible d'entraîner une collecte et un échange de certaines données entre les réseaux sociaux et les BDE du CESI.<br/>
                Nous ne sommes pas responsables de l'utilisation qui est faite de vos données par les réseaux sociaux pour leur propre compte. Vous avez 
                la possibilité de paramétrer et contrôler directement sur les réseaux sociaux l'accès et la confidentialité de vos données.</p>

            <h2>Droits</h2>
            <p>En application de la réglementation Européenne en vigueur, nous avons mis en place des pricédures et mécanismes vous permettant d'execer 
                vos droits.<br/>
                Conformément à la réglementation en vigueur, les Utilisateurs de notre site disposent des droits suivants :</p>
            <ul>
                <li>Droit d'accès et de rectification ;</li>
                <li>De mise à jour, de complétude des données Utilisateurs ;</li>
                <li>Droit de verrouillage ou de suppression des données des Utilisateurs à caractère personnel, lorsqu'elles sont inexactes, incomplètes, 
                    équivoques, périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite ;</li>
                <li>Droit de retirer à tout moment un consentement ;</li>
                <li>Droit à la limitation du traitement des données des Utilisateurs ;</li>
                <li>Droit d'opposition aux traitements des données personnelles ;</li>
                <li>Droit à la portabilité des données que les Utilisateurs auront fourniées, lorsque ces données font l'objet de traitements 
                    automatisés fondés sur leur consentement ou un contrat.</li>
            </ul>

            <p>Si vous souhaitez savoir comment les BDE du CESI utilisent ces données personnelles, demander à les rectifier ou s'opposer à un traitement, 
                vous pouvez envoyer un mail à l'adresse BDEcil@cesi.fr ou adresser un courrier à l'adresse suivante : BDE CESI National - Data Protection 
                Officer - 30, Rue Cambronne - 75015 PARIS - FRANCE. Enfin, les Utilisateurs des BDE du CESI peuvent déposer une réclamation auprès des 
                autorités de contrôle, et notamment de la CNIL. Vos requêtes seront traitées sous 30 jours. En complément de voter demande, nous vous 
                demanderons de joindre une photocopie d'un justificatif d'identité afin que les BDE du CESI puissent vérifier votre identité.</p>

            <h2>Nous contacter</h2>
            <p>Si vous avez des questions ou des réclamations ou si vous souhaitez faire part à un des BDE du CESI de recommandations ou des commentaires 
                visant à améliorer notre Politique de Protection des Données Personnelles, vous pouvez envoyer un email à l'adresse BDEcil@cesi.fr ou 
                adresser un courrier à l'adresse suivante : BDE CESI National - Data Protection Officer - 30, Rue Cambronne - 75015 PARIS - FRANCE.</p>

    </main>

<?php include("./layout/footer.php")?>