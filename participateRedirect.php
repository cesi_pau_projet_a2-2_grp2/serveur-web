<?php
if (isset($_GET["action"]) || isset($_GET["event"])) {
    $action = $_GET["action"];
    $event = $_GET["event"];

    require_once("data_access.php");

    if ($action == "add") {
        
    $content = json_encode(["token" => $_COOKIE["BDE_token"]]);

        EasyCURL::post("/event/$event/participants",$content);
        header("Location: ./eventsDetails.php?id=$event");
        
    }
    if ($action == "remove") {
        
        $content = json_encode(["token" => $_COOKIE["BDE_token"]]);

        EasyCURL::delete("/event/$event/participants",$content);
        header("Location: ./eventsDetails.php?id=$event");
        
    }
}



?>