<?php 
if (isset($_POST["surname"], $_POST["name"], $_POST["centre"], $_POST["email"], $_POST["password"], $_POST["confirm"])){
    $surname = $_POST['surname'];
    $name = $_POST['name'];
    $centre = $_POST['centre'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirm = $_POST['confirm'];

    require_once("data_access.php");
/**
 * We verify if the two passwords filled are exactly the same
 */
        $counterCapitals = 0; // Initialize the capital counter to 0
        $counterInt =0; // Initialize the integer counter to 0
    foreach (str_split($password) as $char) {
        if (ctype_upper($char)) {
            $counterCapitals ++ ; // If the password has a capital letter, the counter is increased by 1
        } if (ctype_digit($char)) {
            $counterInt++; // If the password has an integer, the counter is increased by 1
        }
    }
    if ($counterCapitals >= 1 && $counterInt >= 1) { 
        // If the password is filled correctly, we send the data to the database and redirect the user to the signing in page
        $content = json_encode(["surname"=>$surname, "name"=>$surname, "campusID"=>$centre, "email"=>$email, "password"=>$password]);
        EasyCURL::post("/account",$content);
        header("Location: ./Sign%20in.php"); 
    } else {
        header("Location:./Sign%20up.php"); // Else, the user is redirected to the sign up page with an error message
        echo "Le mot de passe doit contenir au moins une majuscule et un chiffre";
    }
}
    ?>