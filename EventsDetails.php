<?php
$title = "Détails d'une manifestation";
//$nom = "Jean-Michel Générique";
include ("./layout/header_nav.php");
include ("./DownloadGlobal.php");
?>
    
    <main>
        <?php if (!isset($_GET["id"])) { ?> Veuillez renseigner l'id de l'événement à visionner dans l'URL, merci. <?php } ?>

        <?php $event = DAO::get_event($_GET["id"]); setlocale(LC_ALL, "fr_FR") ?>
        <a href="EventsList.php"><input type="submit" value="◀">Liste des manifestations</a>
        <h2><?= $event->event_title ?></h2><!-- Shows the title of the event picked up from the database-->
        <?php $date = date_timestamp_get(date_create($event->event_date)); ?>
        <p><?= ($date > time() ? "Prévue pour" : "A pris fin") ?> le <?= strftime("%A %d %B %Y",$date) ?> à <?= date("H:i",$date) ?><br/>
            Lieu : <?= $event->event_place ?> <br/><!-- Shows the date picked up from the database-->
            Frais de participation : <?= $event->event_price ?>€</p><!-- Shows the price picked up from the database-->
            
        <p><?= str_replace("\n","<br/>",$event->event_desc) ?></p><!-- Shows the description picked up from the database-->
        <!-- The previous lines show the all the data from one event, found by its informations on the database : name, date, location, price and description-->
            <div class="row">
                <div class="col-md-9">
            <?php 
            $date = $event->event_date;
            $ended = time()>$date;
            if (($ended && DAO::$perm_level == 1) || DAO::$perm_level == 2 || ($ended && DAO::$perm_level == 3) || DAO::$perm_level == 4) {?>
            <!-- Allow BDE members and admins to add a picture of the event whenever they want, but students and CESI's employees can do it only
            if the event is finished and these people have participated to this particular event -->
            <label>Ajouter une photo : </label><br/>
            <input type="file" accept="image/*">
            <input type="submit" value="Ajouter la photo">
            <?php } ?>
            <div class="row">
                <div class="col">
                    <a href="./PicDetails.php?1"><img src="./assets/test.png" /></a><br/>
                    <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?><!-- Allow only BDE members and admins to delete a picture -->
                        <button>Supprimer la photo</button>
                    <?php } ?>
                    <?php if (($ended && DAO::$perm_level == 1) || ($ended && DAO::$perm_level == 3) || DAO::$perm_level == 4) {?>
                    <!-- Allow admins whenever they want and only students and CESI members who have participated at this particular event to like and 
                    comment the pictures -->
                    <i class="far fa-thumbs-up"></i>35
                    <i class="far fa-comment-dots"></i>6
                    <?php } ?>
                </div>

                <div class="col">
                    <a href="./PicDetails.php?2"><img src="./assets/test.png" /></a><br/>
                    <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?>
                        <button>Supprimer la photo</button>
                    <?php } ?>
                    <?php if (($ended && DAO::$perm_level == 1) || ($ended && DAO::$perm_level == 3) || DAO::$perm_level == 4) {?>
                    <i class="far fa-thumbs-up"></i>10
                    <i class="far fa-comment-dots"></i>2
                    <?php } ?>
                </div>

                <div class="col">
                    <a href="./PicDetails.php?3"><img src="./assets/test.png" /></a><br/>
                    <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?>
                        <button>Supprimer la photo</button>
                    <?php } ?>
                    <?php if (($ended && DAO::$perm_level == 1) || ($ended && DAO::$perm_level == 3) || DAO::$perm_level == 4) {?>
                    <i class="far fa-thumbs-up"></i>35
                    <i class="far fa-comment-dots"></i>6
                    <?php } ?>
                </div>
                
                <div class="col">
                    <a href="./PicDetails.php?4"><img src="./assets/test.png" /></a><br/>
                    <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?>
                        <button>Supprimer la photo</button>
                    <?php } ?>
                    <?php if (($ended && DAO::$perm_level == 1) || ($ended && DAO::$perm_level == 3) || DAO::$perm_level == 4) {?>
                    <i class="far fa-thumbs-up"></i>10
                    <i class="far fa-comment-dots"></i>2
                    <?php } ?>
                </div>
            </div>
        </div>
        <aside class="col-md-3">
            <h2>Étudiants inscrits</h2> <!-- Allow students, BDE members and admins to join the event and download the list -->
            <?php if (DAO::$perm_level == 1 || DAO::$perm_level == 2 || DAO::$perm_level == 4) {?> 
            <a href="./participateRedirect.php?event=<?= $event->id_event ?>&action=add"><button>Rejoindre</button></a>
            <?php } ?>
            <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?>
            <aside>
                <label>Télécharger la liste au format...</label> <!-- Allow the BDE members and admins to download the list at CSV or PDF format -->
                <a href="DownloadGlobal.php?filename1=Participants.csv"><button>CSV</button></a>
                <a href="DownloadGlobal.php?filename2=Participants.pdf"><button>PDF</button></a>
            </aside>
            <?php } ?>
            <ul>
            <?php foreach ($event->participants as $participant) { ?>
            <!-- Shows the list of people who participates to the event, by picking up the data from the database -->
                <li><?= $participant->surname ?> <?= $participant->name ?></li>
            <?php } ?>
            </ul>
        </aside>
        </div>
        <?php
                $filename1 = "Participants.csv"; // The name of the file in the downloading window
                $dir="http://10.64.128.93/phpmyadmin/sql.php?server=1&db=a2_projet_2_test&table=joins&pos=0".$filename1; // The directory from where we get the pictures
                $filename2 = "Participants.pdf"; // The name of the file in the downloading window
                $dir="http://10.64.128.93/phpmyadmin/sql.php?server=1&db=a2_projet_2_test&table=joins&pos=0".$filename2; // The directory from where we get the pictures
                ?>
    </main>

<?php include("./layout/footer.php")?>