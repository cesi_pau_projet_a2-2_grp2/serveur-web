<?php 
$title = "Inscription";
include("./layout/header_no_nav.php") ;
?>
<div class="row" id="general_row">
        <section id="left_box" class="col-md">
            <div>
            <h1>Déjà inscrit&nbsp;?</h1>
            <a href="Sign%20in.php"><button>CONNECTEZ-VOUS</button></a>
            </div>
        </section>
        <section id="right_box" class="col-md">
            <form id="form" name ="form" novalidate method="POST" action="/SignUpRedirect.php" onsubmit="return validateForm()">
                <h1>Inscrivez-vous</h1>
                <label for="surname">À qui avons-nous affaire&nbsp;?</label>
                <p><input type="text" name="surname" required id="surname" placeholder="Votre nom"/><span class="error" id="wrong_surname"></span><br/>
                <input name="name" required id="name" type="text" placeholder="Votre prénom"/><span class="error" id="wrong_name"></span></p>
                <!-- The keyword required expects the input to be filled by the user in order to validate the form -->
                <p><label for="centre">Centre</label><br/>
                <select name="centre" required id="centre" value="0">
                <span class="error" id="centre_error"></span>
                    <option disabled selected>(Sélectionnez un centre)</option> <!-- Allow a menu where all centres are grouped by region -->
                    <optgroup label="Région Nord-Ouest"> <!-- We cannot select the region -->
                        <option value="3">Arras</option> <!-- But we can select the city -->
                        <option value="6">Caen</option>
                        <option value="11">Lille</option>
                        <option value="21">Rouen</option>
                    </optgroup>
                    <optgroup label="Région Ouest">
                        <option value="2">Angoulême</option>
                        <option value="5">Brest</option>
                        <option value="9">La Rochelle</option>
                        <option value="10">Le Mans</option>
                        <option value="15">Nantes</option>
                        <option value="22">Saint-Nazaire</option>
                    </optgroup>
                    <optgroup label="Région Île-De-France - Centre">
                        <option value="18">Paris (La Défense - Nanterre)</option>
                        <option value="17">Orléans</option>
                    </optgroup>
                    <optgroup label="Région Est">
                        <option value="7">Dijon</option>
                        <option value="14">Nancy</option>
                        <option value="20">Reims</option>
                        <option value="23">Strasbourg</option>
                    </optgroup>
                    <optgroup label="Région Sud-Ouest">
                        <option value="4">Bordeaux</option>
                        <option value="13">Montpellier</option>
                        <option value="19">Pau</option>
                        <option value="24">Toulouse</option>
                    </optgroup>
                    <optgroup label="Région Sud-Est">
                        <option value="1">Aix-en-Provence</option>
                        <option value="8">Grenoble</option>
                        <option value="12">Lyon</option>
                        <option value="16">Nice</option>
                    </optgroup> 
                </select>
                </p>

                <label for="email">Adresse e-mail</label>
                <p><input type="email" required id="email" name="email" placeholder="Votre adresse e-mail">
                <span class="error" id="wrong_mail"></span></p>
                <label for="password">Mot de passe</label>
                <p><input type="password" required id="password" name="password" placeholder="Votre mot de passe"/>
                <span class="error" id="password_error"></span><br/>
                <input type="password" required id="confirm" name="confirm" placeholder="Confirmez votre mot de passe"/>
                <span class="error" id="confirm_error"></span></p>
                <!-- The class error shows us a red box if it is not filled or not filled with expected values-->
                <input type="checkbox" required id="privacy_policy"/><label for="privacy_policy" style="vertical-align : middle">Je reconnais avoir lu et accepte les 
                    <a href="LegalNotice.php">Mentions Légales</a><br/>
                    ainsi que la <a href="PrivacyPolicy.php">Politique de Protection des Données Personnelles</a></label><br/>
                    <!-- The user is expected to accept the Privacy Policy and Legal Notice before signing up. If not, he cannot sign up. -->
                <input type="submit" value="INSCRIVEZ-VOUS"/>
            </form>
        </section>
        </div>
<script src="./assets/VerifySigningUp.js"></script>
<?php include("./layout/footer.php") ?>