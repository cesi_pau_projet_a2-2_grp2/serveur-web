<?php
if (isset($_POST["category"])) {
    $category = $_POST["category"]; // The POST parameter is put into a variable 

    require_once("data_access.php");

    
        
    $content = json_encode(["token" => $_COOKIE["BDE_token"], "name"=>$category]); // Translates the array into a JSON file which will be submitted to the API

        EasyCURL::post("/category",$content); // Add a category to the database
        header("Location: ./products.php"); // Shows the page products to the user
        
    
}
?>