<?php
$title = "Confirmation de commande";
include ("./layout/header_nav.php");
?>
             
    <main>
        <h2>Le BDE vous remercie pour votre commande !</h2>
        <p>Un membre du BDE vous contactera bientôt par e-mail pour prendre rendez-vous</p>

        <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Image</th>
                        <th>Libellé</th>
                        <th>Quantité</th>
                        <th>Prix Unitaire</th>
                        <th>Prix total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><i class="fas fa-times"></i></td>
                        <td><img></td>
                        <td>Clé USB CESI 1Mo</td>
                        <td>- | 1 | +</td>
                        <td>99,00€</td>
                        <td>99,00€</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-times"></i></td>
                        <td><img></td>
                        <td>Bonnet WEI</td>
                        <td>- | 2 | +</td>
                        <td>12,00€</td>
                        <td>24,00€</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-times"></i></td>
                        <td><img></td>
                        <td>1 mois Micro-ondes Nitro</td>
                        <td>- | 1 | +</td>
                        <td>29,99€</td>
                        <td>29,99€</td>
                 </tr>
                </tbody>
            </table><br/>

        <h3>TOTAL</h3>
        <aside>
            <h3>152,99 €</h3>
        </aside>
    </main>

<?php include("./layout/footer.php")?>