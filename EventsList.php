<?php
$title = "Liste des manifestations";
include ("./layout/header_nav.php");
?>
    <main>
        <input type="search" placeholder="Rechercher..."><input type="submit" value="🔎"><br/>
        <input type="checkbox" id="finished_events"><label for="finished_events">Voir les manifestations terminées</label><br/>
        <select>
            <option disabled selected>Trier par prix</option>
            <option>Prix croissant</option>
            <option>Prix décroissant</option>        
        </select>
        <select>
            <option disabled selected>Trier par nom</option>
            <option>De A à Z</option>
            <option>De Z à A</option>
        </select>
        <?php 
            $list = DAO::get_events(); //We get allt he events from the database
            setlocale(LC_ALL, "fr_FR");
            $is_ofTheMonth = true; //We set the event as the Event of the Month
        ?>
        <?php foreach ($list as $event) {?><!-- Put the event from the database -->
        <section <?= $is_ofTheMonth ? 'class="ofTheMonth"' : ""?>>
        <?php if ($is_ofTheMonth) { ?><h1>Événement du mois</h1> <?php $is_ofTheMonth = false; } ?> 
        <!-- If the event pulled from the database is the Monthly Event, then it is shown on the part Event of the Month-->
            <a href="./eventsdetails.php?id=<?= $event->id_event ?>"><h2><?= $event->event_title ?></h2></a>
            <aside>
                <?php if (DAO::$perm_level == 2 || DAO::$perm_level == 4) {?> <!-- Allow BDE members and admins to delete the event-->
                <button><i class="fas fa-times"></i>Supprimer</button>
                <?php } ?>
                <?php if (DAO::$perm_level >= 3) {?> <!-- Allow CESI employees and admins to delete the event-->
                <a href="report.php"><button><i class="fas fa-flag"></i>Signaler</button></a>
                <?php } ?>
            </aside>
            <p><?= $event->event_desc ?><br/>
                Prévue le <?= strftime("%A %d %B %Y",date_timestamp_get(date_create($event->event_date))) ?>      Participation&nbsp;: <?= $event->event_price ? $event->event_price."€" : "gratuite" ?> <?= -1//count($event->participants) ?>👥  <?= -1//count($event->pictures) ?>📸</p>
        </section>
        <?php } ?>
    </main>

<?php include("./layout/footer.php")?>