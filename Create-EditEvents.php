<?php
$title = "Ajout d'une manifestation";
include ("./layout/header_nav.php");
?>
<!-- Creates a form to create an event. All areas with expected filled values are described as required.
If the form is not fullfilled or not filled with expected values, the event cannot be created.-->
    <main>
        <form id="eventForm" name="eventForm" novalidate action="./CreateEventRedirect.php" onsubmit="return validateEventForm()">
            <p><label for="event_name">Nom *</label><br/>
            <input type="text" name="event_name" required id="event_name" placeholder="Nom de la manifestation"/><span class="error" id="event_name_error"></span></p>
            
            <p><label for="event_location">Lieu</label><br/>
            <input type="text" id="event_location" placeholder="Adresse"/></p>

            <p><label for="date">Date *</label><br/>
            <input type="date" required id="date"/><span class="error" id="event_date_error"></span></p>
            
            <p><label for="hour">Heure *</label><br/>
            <input type="time" required id="hour"/><span class="error" id="event_hour_error"></span></p>
            
            <p><label for="cost">Frais de participation *</label><br/>
            <input type="number" required id="cost" min="0" step="0.5"/>€<span class="error" id="event_cost_error"></span></p>
            
            <p><label for="description">Description</label><br/>
            <textarea id="description"  rows="5" cols="60" placeholder="Décrivez la manifestation"></textarea></p>
            <h3>Photos</h3>
            Ajouter une image<br/>
            <input type="file">
            <img src="./assets/test.png"/>
            <img src="./assets/test.png"/>
            <img src="./assets/test.png"/>
            <p><input type="submit" value="Ajouter la manifestation"/></p>
        </form>
    </main>
    
<script src="./assets/VerifyCreateEditEvent.js"></script>
<?php include("./layout/footer.php")?>