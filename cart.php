<?php
$title = "Panier";
include ("./layout/header_nav.php");
$emptyCart = false;
if (!isset($_COOKIE["BDE_cart"])) $emptyCart = true;

if (!$emptyCart) {
    $cart = json_decode($_COOKIE["BDE_cart"]);
    if ($cart == json_decode("{}")) $emptyCart = true;
}

?>
            
    <main>
    <?php if (!$emptyCart) { ?>
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Libellé</th>
                    <th>Quantité</th>
                    <th>Prix Unitaire</th>
                    <th>Prix total</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($cart as $item) { ?>
                <tr>
                    <td><button><i class="fas fa-times"></i></button></td>
                    <td></td>
                    <td><?= $item->name ?></td>
                    <td><button>-</button> | <?= $item->qty ?> | <button>+</button></td>
                    <td><?= $item->price ?></td>
                    <td><td><?= $item->price * $item->qty ?></td></td>
                </tr>
            <?php } ?>
                <!-- <tr>
                    <td><button><i class="fas fa-times"></i></button></td>
                    <td><img></td>
                    <td>Bonnet WEI</td>
                    <td><button>-</button> | 2 | <button>+</button></td>
                    <td>12,00€</td>
                    <td>24,00€</td>
                </tr>
                <tr>
                    <td><button><i class="fas fa-times"></i></button></td>
                    <td><img></td>
                    <td>1 mois Micro-ondes Nitro</td>
                    <td><button>-</button> | 1 | <button>+</button></td>
                    <td>29,99€</td>
                    <td>29,99€</td>
             </tr> -->
            </tbody>
        </table><br/> <!-- Ask the customer to accept the General Conditons of Selling to follow its order. A link is added if the customer wants to read theses conditions. -->
        <input type="checkbox" required id="CGV"><label for="CGV">Je reconnais avoir lu et accepte les <a href="CGV.php">Conditions générales de vente</a></label>
        <p><a href="endOfBuy.php"><button>COMMANDER</button></a></p>

    <?php } else { ?>
        <h1>Votre panier est vide.</h1>
    <?php } ?>
    </main>

<?php include("./layout/footer.php")?>