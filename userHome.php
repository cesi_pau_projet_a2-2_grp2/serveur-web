<?php
$title = "Accueil";
include ("./layout/header_nav.php");
?>

<section>
    <h1>Bienvenue<?= DAO::$perm_level>0 ? ", ".DAO::$name :" "?></h1>
    <!-- This line put form the database the name of the owner of the account using its  id. Then, it shows the right name on the Web page. -->
</section>
<?php foreach (DAO::get_notifications() as $notif) { ?>
    <label><?= $notif->content ?></label><br/>
<?php } ?>
<section>
    <div class="notification-success">Votre idée de manifestation "Soirée bowling" a été retenue<br/> <!-- Shows a success notification, like an accepted idea as an event -->
        Voir la manifestation
        <aside><button><i class="fas fa-times"></i>Supprimer</button></aside>
    </div>
    <div class="notification-warning">Une photo postée sur "CESI Cup" a été signalée comme inapropriée, raison : "Lorem ipsum" <!-- Shows a warning notification, like a report -->
        <aside><button><i class="fas fa-times"></i>Supprimer</button></aside> <!-- Allow to delete the notification -->
    </div>
</section>

<article>
    <h2>Voici les événements les plus populaires des BDE des centres CESI</h2>
    <h3>CESI Cup</h3>
    <img src="./assets/test.png"/>
    <p>La CESI Cup regroupe les différentes branches d'un même centre CESI autour de différentes activités :</p>
    <ul>
        <li>Activités sportives</li>
        <li>Jeux en équipes</li>
        <li>Parcours du combattant</li>
        <li>Quiz de culture générale</li>
        <li>Épreuves de dégustation</li>
        <li>Matches de logique</li>
        <li>Et autres activités...</li>
    </ul>
    <p>Toutes les équipes s'affronteront lors d'une demi-journée avec à la clé pour l'équipe vainqueure, une montagne de cadeaux !</p>
    <p>La CESI Cup est organisée par le BDE de chaque centre CESI, et la participation des étudiants est gratuite et facultative.</p>
</article>
<article>
    <h3>Césiades</h3>
    <img src="./assets/test.png"/>
    <p>Les Césiades sont une version un peu plus nationale de la CESI Cup, où tous les centres de France s'affrontent autour d'épreuves sportives :</p>
    <ul>
        <li>Match de football</li>
        <li>Concours de pétanque</li>
        <li>Tir à l'arc</li>
        <li>Match de basketball</li>
        <li>Match de handball</li>
        <li>Tir à la corde</li>
        <li>Sprint sur 100 m</li>
        <li>Et bien d'autres épreuves...</li>
    </ul>
    <p>Chaque centre désigne une équipe de 15 étudiants qui se rendent dans la ville du centre CESI vainqueur en titre afin de s'affronter dans des 
        épreuves toutes plus physiques les unes que les autres.</p>
    <p>Les Césiades sont financées par les BDE de tous les centres CESI. Tous les étudiants peuvent s'inscrire dans la liste qui représentera leur centre.
        Les inscrits et sélectionnés voient leur participation devenue obligatoire.</p>
</article>
<article>
    <h3>Week-end d'intégration</h3>
    <img src="./assets/test.png"/>
    <p>Le week-end d'intégration permet aux étudiants de première année de passer un weekend afin de rencontrer les autres étudiants d'un centre CESI.
        Au cours de ce week-end sont prévus :</p>
    <ul>
        <li>Activités de présentation</li>
        <li>Activités sportives</li>
        <li>Jeux en tout genre</li>
        <li>Activités aquatiques si beau temps</li>
        <li>Activités prévues si mauvais temps</li>
        <li>Déjeuners, dîner et petit-déjeuner inclus</li>
        <li>Soirée musique avec les autres étudiants</li>
    </ul>
    <p>L'organisation des week-end d'intégration est à la charge entière des BDE des centres CESI. La participation de tous les étudiants est 
        facultative, y compris pour les étudiants de première année. Les frais de participation varieront suivant les BDE des centres CESI.</p>
</article>

<?php include("./layout/footer.php")?>