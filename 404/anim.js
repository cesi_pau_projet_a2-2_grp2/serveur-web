var anim = 0;
setInterval(() => {
    var texte = " ERREUR HTTP ";
    for (let i = 0; i < 4; i++) {
        texte = (i == anim || i == anim - 1 ? " " : "<") + texte + (i == anim || i == anim - 1 ? " " : ">");
    }
    titre = document.getElementById("titre");
    titre.innerHTML = texte;
    anim++;
    anim %= 5;
    
    document.getElementById("code").innerHTML = Math.random() <= 0.05 ? Math.random().toString().substr(2, 3) : "404";
 },100)