<?php
if (isset($_POST["idea"])) {
    $idea = $_POST["idea"]; // The POST parameter is put into a variable 

    require_once("data_access.php");

    
        
    $content = json_encode(["token" => $_COOKIE["BDE_token"], "desc"=>$idea]); // Translates the array into a JSON file which will be submitted to the API

        EasyCURL::post("/idea",$content); // Delete a category from the database
        header("Location: ./IdeasBox.php"); // Shows the right page to the user
        
    
}
?>